"""Represents the move to goal behaviour"""
import navigation as nav
import math
from behaviour import Behaviour


class MoveToGoal(Behaviour):
    """Defines the move to goal behaviour"""

    def activate(self):
        """Initialize the routine"""
        alpha = 2
        while self.run:

            robot_position = nav.get_robot_position()

            angle_from_goal = nav.get_angle_from_goal(robot_position)
            changing_angle = nav.get_robot_orientation() - angle_from_goal
            vX = alpha * (nav.get_distance_from_goal(robot_position)) * math.cos(
                nav.get_angle_from_goal(robot_position))
            vY = alpha * (nav.get_distance_from_goal(robot_position)) * math.sin(
                nav.get_angle_from_goal(robot_position))

            vfx = math.sqrt(math.pow(vX, 2) + math.pow(vY, 2)) + changing_angle
            vfy = math.sqrt(math.pow(vX, 2) + math.pow(vY, 2)) - changing_angle
            return vfx, vfy

        else:
            self.run = False
            return 0, 0

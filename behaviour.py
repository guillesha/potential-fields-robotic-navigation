"""Superclass for all the behavirous"""
import navigation as nav


class Behaviour:
    """Includes the methods that all the behaviours use"""

    def __init__(self):
        self.run = False

    def stop(self):
        """Stops the behaviour"""
        self.run = False
        return False

    def run_behaviour(self):
        """Starts the behaviuor"""
        self.run = True

    def is_in_goal(self):
        robot_position = nav.get_robot_position()
        if nav.get_distance_from_goal(robot_position) != 0 and nav.get_distance_from_goal(robot_position) < 0.2:
            return True
        return False

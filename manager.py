# Make sure to have the server side running in V-REP:
# in a child script of a V-REP scene, add following command
# to be executed just once, at simulation start:
#
# simRemoteApi.start(19999)
#
# then start simulation, and run this program.
#
# IMPORTANT: for each successful call to simxStart, there
# should be a corresponding call to simxFinish at the end!

import math
from handlers import *
import constants
import time
import navigation as nav
from move_to_goal import MoveToGoal
from obstacle_reject import ObstacleReject
from avoidpast import AvoidPast


def exit_handler():
    """Stops the simulation when the program stops"""
    vrep.simxStopSimulation(clientID, vrep.simx_opmode_blocking)


atexit.register(exit_handler)
if constants.clientID != -1:
    print('Connected to remote API server')
    # left motor of the robot
    routine_1 = MoveToGoal()
    routine_2 = ObstacleReject()
    routine_3 = AvoidPast()

    while not routine_1.is_in_goal():

        routine_1.run_behaviour()
        routine_2.run_behaviour()
        routine_3.run_behaviour()
        attractive_force = routine_1.activate()
        reject_force = routine_2.activate()

        if attractive_force is not None and reject_force is not None:
            vrep.simxAddStatusbarMessage(clientID, "U=" + str(abs(abs(attractive_force[0]) - abs(reject_force[0]))),
                                         vrep.simx_opmode_blocking)

            routine_3.remember(abs(attractive_force[0]) - abs(reject_force[0]))
            avoid_past = routine_3.move_to_avoid_past_position(abs(attractive_force[0]) - abs(reject_force[0]))
            print(avoid_past)
            if avoid_past is False:
                nav.set_robot_velocity(attractive_force[0] + reject_force[0], attractive_force[1] + reject_force[1])
            else:
                vrep.simxAddStatusbarMessage(clientID, "Avoid past activated \n return to free point", vrep.simx_opmode_blocking)
                robot_position = nav.get_robot_position()
                while nav.get_distance_from_point(robot_position, routine_3.free_position) > 0.2:
                    robot_position = nav.get_robot_position()
                    direction = routine_3.go_to_position()
                    nav.set_robot_velocity(direction[0], direction[1])
                new_path = routine_3.take_other_path()
                vrep.simxAddStatusbarMessage(clientID, "Take new path", vrep.simx_opmode_blocking)
                while new_path == (0, 3):
                    new_path = routine_3.take_other_path()
                    nav.set_robot_velocity(new_path[0], new_path[1])
                if new_path == (5, 5):
                    print("5,5")
                    nav.set_robot_velocity(new_path[0], new_path[1])
                    time.sleep(3)
            time.sleep(1)
    vrep.simxAddStatusbarMessage(clientID, "Goal reached",vrep.simx_opmode_blocking)


else:
    print('Failed connecting to remote API server')

"""Behaviour"""
import navigation as nav
import vrep
from constants import clientID
from handlers import *
import math
from behaviour import Behaviour
import time


class AvoidPast(Behaviour):
    """The behaviour is effective for local minimums
     it remmbers a past free position and brings the robot to it"""

    def __init__(self):
        super().__init__()
        self.free_position = (0, 0)
        self.proximity_left = (0, 0)
        self.proximity_right = (0, 0)
        self.proximity_front = (0, 0)

    def remember(self, u):
        """Associates a last position where sensors didn't detect and obstacle"""
        robot_position = nav.get_robot_position()
        while self.run:
            self.proximity_left = vrep.simxReadProximitySensor(clientID, sensor_handle4, vrep.simx_opmode_streaming)
            self.proximity_right = vrep.simxReadProximitySensor(clientID, sensor_handle8, vrep.simx_opmode_streaming)
            self.proximity_front = vrep.simxReadProximitySensor(clientID, sensor_handle1, vrep.simx_opmode_streaming)

            if not self.proximity_right[1] and not self.proximity_front[1] and not self.proximity_left[1] and u > 2:
                # if the path is free of obstacles remembers this position
                self.free_position = robot_position
                print(self.free_position)
                return
            return

    def move_to_avoid_past_position(self, u):
        """Moves the robot to last remembered free position"""

        print("avoid past")
        if self.proximity_right[1] and self.proximity_front[1] and self.proximity_left[1] and u < 0.5:
            return True

        else:
            # the activation is not needed
            return False

    def go_to_position(self):
        robot_position = nav.get_robot_position()
        changing_angle = math.degrees(nav.get_robot_orientation()) - math.degrees(
            nav.get_angle_from_point(robot_position, self.free_position))
        if abs(changing_angle) > 0:
            if abs(changing_angle) < 20:

                return 5, 5
            else:
                return 0, 3

    def take_other_path(self):
        robot_position = nav.get_robot_position()
        print("other_path")
        changing_angle = math.degrees(nav.get_robot_orientation()) - math.degrees(
            nav.get_angle_from_goal(robot_position))
        if abs(changing_angle) > 0:
            print(changing_angle)
            if abs(changing_angle) > 85 and abs(changing_angle) < 95:
                return 5, 5
            else:
                return 0, 3

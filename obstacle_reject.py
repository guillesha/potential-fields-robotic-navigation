"""The class represents a behaviour for the rejecting potential on the obstacles"""
from behaviour import Behaviour
from handlers import *

import navigation as nav
import math


class ObstacleReject(Behaviour):
    def obstacle(self):
        """returns wheter the obstacle shoud be considered or no"""

    def activate(self):
        beta = 4
        while self.run:
            robot_position = nav.get_robot_position()
            distance = nav.get_distance_from_obstacle(robot_position)
            proximity_left = vrep.simxReadProximitySensor(clientID, sensor_handle4, vrep.simx_opmode_streaming)
            proximity_right = vrep.simxReadProximitySensor(clientID, sensor_handle6, vrep.simx_opmode_streaming)
            proximity_front = vrep.simxReadProximitySensor(clientID, sensor_handle5, vrep.simx_opmode_streaming)

            if distance < 3 and (proximity_front[1] or proximity_left[1] or proximity_right[
                1]):

                changing_angle = nav.get_robot_orientation() - nav.get_angle_from_obstacle(robot_position)

                vX = beta / distance * math.cos(
                    nav.get_angle_from_obstacle(robot_position))
                vY = beta / distance * math.sin(
                    nav.get_angle_from_obstacle(robot_position))

                vfx = math.sqrt(math.pow(vX, 2) + math.pow(vY, 2)) + changing_angle
                vfy = math.sqrt(math.pow(vX, 2) + math.pow(vY, 2)) - changing_angle

                return -abs(vfx), -abs(vfy)
            else:
                self.run = False
                return 0, 0

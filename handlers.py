"""Declare handlers for the simulation"""

try:
    import vrep
except:
    print('--------------------------------------------------------------')
    print('"vrep.py" could not be imported. This means very probably that')
    print('either "vrep.py" or the remoteApi library could not be found.')
    print('Make sure both are in the same folder as this file,')
    print('or appropriately adjust the file "vrep.py"')
    print('--------------------------------------------------------------')
    print('')

import atexit
from constants import clientID, Robot

print('Program started')
vrep.simxFinish(-1)  # just in case, close all opened connections
clientID = vrep.simxStart('127.0.0.1', 19997, True, True, 5000, 5)  # Connect to V-REP
error_code, left_motor = vrep.simxGetObjectHandle(clientID, "Pioneer_p3dx_leftMotor",
                                                  vrep.simx_opmode_blocking)

error_code2, right_motor = vrep.simxGetObjectHandle(clientID, "Pioneer_p3dx_rightMotor",
                                                    vrep.simx_opmode_blocking)
error_code3, floorHandle = vrep.simxGetObjectHandle(clientID, 'ResizableFloor_5_25',
                                                    vrep.simx_opmode_blocking)

error_code4, robot_handle = vrep.simxGetObjectHandle(clientID, Robot,
                                                     vrep.simx_opmode_blocking)
error_code5, position = vrep.simxGetObjectPosition(clientID, robot_handle, floorHandle, vrep.simx_opmode_streaming)
vrep.simxStartSimulation(clientID, vrep.simx_opmode_streaming)

error_code8, sensor_handle3 = vrep.simxGetObjectHandle(clientID, "Pioneer_p3dx_ultrasonicSensor3",
                                                       vrep.simx_opmode_blocking)
error_code9, sensor_handle4 = vrep.simxGetObjectHandle(clientID, "Pioneer_p3dx_ultrasonicSensor4",
                                                       vrep.simx_opmode_blocking)
error_code10, sensor_handle5 = vrep.simxGetObjectHandle(clientID, "Pioneer_p3dx_ultrasonicSensor5",
                                                        vrep.simx_opmode_blocking)
error_cod11, sensor_handle6 = vrep.simxGetObjectHandle(clientID, "Pioneer_p3dx_ultrasonicSensor6",
                                                       vrep.simx_opmode_blocking)
error_code12, sensor_handle1 = vrep.simxGetObjectHandle(clientID, "Pioneer_p3dx_ultrasonicSensor1",
                                                        vrep.simx_opmode_blocking)
error_cod13, sensor_handle8 = vrep.simxGetObjectHandle(clientID, "Pioneer_p3dx_ultrasonicSensor8",
                                                       vrep.simx_opmode_blocking)




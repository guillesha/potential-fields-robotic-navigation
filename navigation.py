"""Set the methods for the navigation"""
import math
import constants as con
from handlers import *
from constants import clientID


def get_distance_from_goal(robot_position):
    """:return the distance between the robot and the goal"""
    return math.sqrt(
        math.pow(con.Position_Goal[0] - robot_position[0], 2) + math.pow(robot_position[1] - con.Position_Goal[1], 2))


def get_angle_from_goal(robot_position):
    """:return the angle from goal"""
    return math.atan2(con.Position_Goal[1] - robot_position[1], con.Position_Goal[0] - robot_position[0])


def get_angle_from_obstacle(robot_position):
    return math.atan2(-2 - robot_position[1], 0 - robot_position[0])


def get_distance_from_obstacle(robot_position):
    return math.sqrt(
        math.pow(0 - robot_position[0], 2) + math.pow(-2 - robot_position[1], 2))


def set_robot_velocity(v_left, v_right):
    """Sets the robot velocity"""
    error_code_velocity = vrep.simxSetJointTargetVelocity(clientID, left_motor, v_left,
                                                          vrep.simx_opmode_streaming)
    error_code_velocity_right = vrep.simxSetJointTargetVelocity(clientID, right_motor, v_right,
                                                                vrep.simx_opmode_streaming)


def get_robot_position():
    """retunrs the absolute position of the robot"""
    return vrep.simxGetObjectPosition(clientID, robot_handle, -1,
                                      vrep.simx_opmode_streaming)[1]


def get_robot_orientation():
    """Returns the absolute robot orientation"""
    return vrep.simxGetObjectOrientation(clientID, robot_handle, -1, vrep.simx_opmode_streaming)[1][2]


def get_angle_from_point(robot_position, point):
    """Returns the angle from a specific point"""
    return math.atan2(point[1] - robot_position[1], point[0] - robot_position[0])


def get_distance_from_point(robot_position, point):
    """Returns the distance from a point"""
    return math.sqrt(
        math.pow(point[0] - robot_position[0], 2) + math.pow(robot_position[1] - point[1], 2))
